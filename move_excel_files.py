import os
import shutil

def move_excel_files(base_directory):
    # Get a list of all files in the base directory
    for filename in os.listdir(base_directory):
        # Check if the file is an Excel file
        if filename.endswith('.csv'):
            filename_without_extension = os.path.splitext(filename)[0]
            # Extract the respondent ID from the filename
            # Assumes filename format: respondent_'id'_questionnare_answers.xlsx or respondent_'id'.xlsx
            parts = filename_without_extension.split('_')
            respondent_id = parts[1]  # This gets the 'id' part
            
            # Construct the target directory path
            target_directory = os.path.join(base_directory, f'respondent_{respondent_id}')
            
            # Ensure the target directory exists
            if os.path.isdir(target_directory):
                # Construct the full file paths
                source_path = os.path.join(base_directory, filename)
                destination_path = os.path.join(target_directory, filename)
                
                # Move the file
                shutil.move(source_path, destination_path)
                print(f'Moved {filename} to {target_directory}')
            else:
                print(f'Target directory {target_directory} does not exist for {filename}')

# Replace with your base directory path
base_directory = 'data'
move_excel_files(base_directory)
