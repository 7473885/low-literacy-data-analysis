### Dataset Columns and Examples

The dataset contains the following columns:

- `id`: The participant's ID.
- `word`: The word given to the participant.
- `ASR`: The output of the Automatic Speech Recognition (ASR) system.
- `response`: The participant's response.
- `RT`: The response time.
- `error_padding`: Additional error information.
- `Accept` - Did the participant press the accept button (1 / 0, depends on the id)
- `Correct ASR` - Is the output of the ASR the same as the word (1/0)
- `Correct answer` - Is the answer of the participant correct with regards to `Accept` and `Correct ASR` (1/0)
- `experiment` - For every participant the first two words are just a tutorial, we have marked them with `experiment` = 1
- `frequency` - Frequency of the word (-1 - low frequency, 0 - average, 1 - high frequency)

The rules for acceptance and rejection are different for odd and even participant IDs:

- For odd IDs: Accept 'x', Reject 'n'.
- For even IDs: Accept 'n', Reject 'x'.

Here are some examples:

- For odd IDs:
  - `1,sounds\link.mp3,link,x,1.4012596999527889,` - The ASR output is the same as the word, and the participant pressed the correct button.
  - `1,sounds\stamp.mp3,stam,n,1.1050151999806983,` - The ASR output is not the same as the word, and the participant pressed the correct button.
  - `107,sounds\hand.mp3,hand,n,3.0300012440420687e-05` - The ASR output is the same as the word, but the participant pressed the incorrect button (*).
  - `1,sounds\gamba.mp3,Wanda,x,1.193910000308829,` - The ASR output is not the same as the word, and the participant pressed the incorrect button.

- For even IDs:
  - `2,sounds\hand.mp3,hand,n,1.5153536999996504,` - The ASR output is the same as the word, and the participant pressed the correct button.
  - `2,sounds\vamp.mp3,van,x,0.78017170000021,` - The ASR output is not the same as the word, and the participant pressed the correct button.
  - `20,sounds\keur.mp3,keur,x,0.8433842999802437,` - The ASR output is the same as the word, but the participant pressed the incorrect button (*).
  - `2,sounds\pens.mp3,pence,n,1.9623176000000055,` - The ASR output is not the same as the word, and the participant pressed the incorrect button.

Lines marked with (*) are very rare. They indicate cases where the ASR output was correct, but the participant pressed the wrong button.

### Additional Columns in the Dataset

We introduce three new columns in the dataset:

1. `Accept` - This column indicates whether the participant pressed the accept button. It takes values 1 or 0. The value is 1 if the id is odd and the button pressed is 'x', or if the id is even and the button pressed is 'n'. The value is 0 if the id is odd and the button pressed is 'n', or if the id is even and the button pressed is 'x'.

2. `Correct ASR` - This column indicates whether the Automatic Speech Recognition (ASR) output is the same as the word given. It takes values 1 or 0. The value is 1 if the ASR output matches the word, and 0 otherwise.

3. `Correct answer` - This column indicates whether the participant's answer is correct with regards to the word and transcription. It takes values 1 or 0. The value is 1 if the participant's answer is correct, and 0 otherwise.

The `Correct answer` column is calculated as the logical NOT of the XOR of the `Accept` and `Correct ASR` columns. In other words, if the participant's action (accept or reject) matches the correctness of the ASR output (whether the ASR output matches the word), then the answer is considered correct.

Here's the Python code used to calculate these columns:

```python
import numpy as np

# Define a function to calculate the 'Accept' column
def calculate_accept(row):
    if (row['id'] % 2 == 1 and row['response'] == 'x') or (row['id'] % 2 == 0 and row['response'] == 'n'):
        return 1
    else:
        return 0

# Define a function to calculate the 'Correct ASR ?' column
def calculate_correct_asr(row):
    if row['word'].split('\\')[-1].replace('.mp3', '') == row['ASR']:
        return 1
    else:
        return 0

# Apply the functions to the DataFrame to create the new columns
df['Accept'] = df.apply(calculate_accept, axis=1)
df['Correct ASR'] = df.apply(calculate_correct_asr, axis=1)

# Calculate the 'Correct answer ?' column based on the 'Accept' and 'Correct ASR ?' columns
df['Correct answer'] = np.logical_not(df['Accept'] ^ df['Correct ASR']).astype(int)