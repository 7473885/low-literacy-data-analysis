import os
import pandas as pd

def combine_data_files_with_traversal(base_directory, combined_output_file):
    # Define the column names, including the new column 'extra_col'
    column_names = ['id', 'word', 'ASR', 'response', 'RT', 'error_padding']
    
    combined_df = pd.DataFrame(columns=column_names)  # Create an empty DataFrame with defined columns

    # Traverse the directory tree
    for root, dirs, files in os.walk(base_directory):
        for filename in files:
            if filename == 'data.csv':
                file_path = os.path.join(root, filename)
                
                try:
                    # Read the CSV file into a DataFrame without headers and assign the column names
                    df = pd.read_csv(file_path, header=None, names=column_names, encoding='ISO-8859-1')
                    
                    # Append the data to the combined DataFrame
                    combined_df = pd.concat([combined_df, df], ignore_index=True)
                except UnicodeDecodeError as e:
                    print(f'Error reading {file_path}: {e}')
    
    # Save the combined DataFrame to a new CSV file with the header
    combined_df.to_csv(combined_output_file, index=False, header=True)
    print(f'All data combined and saved to {combined_output_file}')

# Replace with your base directory path and desired output file path
base_directory = 'data'
combined_output_file = 'final_combined_data.csv'
combine_data_files_with_traversal(base_directory, combined_output_file)
