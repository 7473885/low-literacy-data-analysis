import os
import pandas as pd

def combine_question_answer_data(base_directory, combined_output_file):
    # Define the column names for the question-answer pairs
    qa_column_names = ['question','answer']
    combined_df = pd.DataFrame(columns=qa_column_names)  # Create an empty DataFrame with defined columns
    # Traverse the directory tree
    for root, dirs, files in os.walk(base_directory):
        for filename in files:
            if filename.endswith('.csv') and filename != 'data.csv' :
                file_path = os.path.join(root, filename)
                try:
                    # Read the CSV file into a DataFrame without headers
                    df = pd.read_csv(file_path, header=None, encoding='ISO-8859-1',skiprows=1,names=qa_column_names)
                    # Extract respondent ID from folder name
                    respondent_id = os.path.basename(root).split('_')[1]
                    # Add respondent ID column
                   # df['id'] = respondent_id
                    df.insert(0, 'id', respondent_id)
                    # Append the data to the combined DataFrame
                    combined_df = pd.concat([combined_df, df], ignore_index=True)
                except UnicodeDecodeError as e:
                    print(f'Error reading {file_path}: {e}')
    
    # Save the combined DataFrame to a new CSV file with the header
    combined_df.to_csv(combined_output_file, index=False, header=True)
    print(f'All question-answer data combined and saved to {combined_output_file}')

# Replace with your base directory path and desired output file path
base_directory = 'data'
combined_output_file = 'final_combined_questionare_data.csv'
combine_question_answer_data(base_directory, combined_output_file)
